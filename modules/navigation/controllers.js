'use strict';

var myCtrl = angular.module('Navigation');

myCtrl.controller('NavigationController' , [ '$scope' , '$rootScope' , '$location', 'MainService' , '$cookies', '$timeout' , function( $scope, $rootScope, $location, MainService, $cookies, $timeout ) {

	$scope.userEmail = MainService.getUserEmail();


// ==== IF USER IS LOGIN LEAD TO HOME PAGE ====
	if(MainService.isUserAuthenticated()) {
		$location.path('/home');
	};


// ==== SHOW NAVIGATION FOR EACH SITE ====
	$scope.navigation = function() {
		return 'modules/navigation/views/navigation.html';
	};


// ==== SHOW PICTURE ONLY IN MAIN PAGE ====
	$scope.$watch( function () {
		if($location.path() === '/navigation') {
			$scope.tlo = true;
		} else {
			$scope.tlo = false;
		}
	});

// ==== WATCH LOCATION AND SHOW/HIDE NAVBAR PANEL DEPENDING ON USERS ====
	$scope.$watch( function (){
		if($location.path() === '/home' || $location.path() === '/register') {
			$scope.success = true;
			$scope.currentUserRole = MainService.getUserRole();
				if($scope.currentUserRole == 'admin') {
					$scope.role = true;
				} else {
					$scope.role = false;
				}
		} else
			$scope.success = false;
	});

// ==== PATH LIGHTING IF IT IS ACTIVE ====
	$scope.isActive = function( path ) { 
	  return $location.path() === path;     
	};


// ==== LEAD TO NAVIGATION ====
	$scope.logout = function() {
		MainService.clearCredentials();
		$location.path('/navigation');
	};

// ==== SHOW UPDATE PASSWORD MODAL ====
	$scope.show = function() {
		$('#update').modal('show');
	};


	$scope.updatePassword = function() {
		if($scope.updatePassNew !== $scope.updatePassConf) {
			$scope.error = 'Passwords are not the same.';
				$timeout(function(){
					$scope.error = "";
				}, 2000 );
		} else {
			// get user email
			$scope.userEmail = MainService.getUserEmail();
			MainService.updatePassword($scope.userEmail, $scope.updatePass, $scope.updatePassNew)
			.then(function successCallback(response) {
				$scope.response = response.data;
					if($scope.response.success) {
						$scope.message = $scope.response.message;
							// delete all inputs after 2s
							$timeout(function(){
								$scope.message = "";
								$scope.updatePass = "";
								$scope.updatePassNew = "";
								$scope.updatePassConf = "";
								$('#update').modal('hide');
							}, 2000 );
					} else {
						$scope.error = $scope.response.message;
							$timeout(function(){
								$scope.error = "";
							}, 3000 );
					}
			}).catch(function errorCallback(response) {
				console.log("Unable to update login.");
			});
		}
	};

}]);



myCtrl.controller('LoginController' , [ '$scope' , '$rootScope' , '$location', 'MainService' , '$cookies', function( $scope, $rootScope, $location, MainService, $cookies ) {

// ==== IF USER IS LOGIN LEAD TO HOME PAGE ====
	if(MainService.isUserAuthenticated()) {
		$location.path('/home');
	};


	$scope.login = function() {
		MainService.Login($scope.userEmail, $scope.userPassword, function(response) {
			$scope.response = response.data;
			if($scope.response.success) {
				MainService.setCredentials($scope.userEmail, response.data.userName, response.data.id, response.data.role);
				MainService.setCookie($scope.userEmail, $scope.response.token, $scope.response.userName, $scope.response.id, $scope.response.role);
				$location.path('/home');
			} else {
				$scope.error = $scope.response.message;
			}
		});
	};
}]);



myCtrl.controller('RegisterController' , [ '$scope' , '$rootScope' , '$location', 'MainService' , '$timeout' , function( $scope, $rootScope, $location, MainService, $timeout ) {

// ==== SHOW REGISTER PANEL WHEN ADMIN WANT TO ADD USERS ====
	$scope.registerSuccess = false;

	$scope.register = function() {
		if($scope.userPassword !== $scope.userPassconf) {
			$scope.errorPassconf = 'Passwords are not the same.';
				$timeout(function(){
					$scope.errorPassconf = "";
				}, 2000 );
		} else {
			MainService.Register($scope.userName, $scope.userEmail, $scope.userPassword, function(response) {
				$scope.response = response.data;
				if($scope.response.success) {
					// hide register panel when admin saved users and show alert success
					$scope.registerSuccess = true;
						$timeout(function(){
						$scope.userName = "";
						$scope.userEmail = "";
						$scope.userPassword = "";
						$scope.userPassconf = "";
						$scope.registerSuccess = false;
					} , 2000 );	
				console.log('Udało sie zapisać');
				} else {
					// show register panel with all errors
					$scope.registerSuccess = false;
					$scope.errorName = $scope.response.errorName;
					$scope.errorEmail = $scope.response.errorEmail;
					$scope.errorPass = $scope.response.errorPass;
					$scope.errorPassconf = $scope.response.errorPassconf;
				console.log('Nie udało sie zapisać');
					// console.log($scope.response);
				}
			})
		};
	};
}]);