'use strict';

angular.module('Navigation')

.factory('MainService' , [ '$http' , '$cookies' , '$location' , function($http, $cookies, $location) { 

  var service = {};

  var currentUser = {
     isUserAuthenticated: false,
     useremail: ""
  };


	service.Login = function (userEmail, userPassword, callback) {
		$http({
          method: 'POST',
          url: 'http://localhost/ideaboardz/api/login.php',
          data: { email: userEmail,
          		    pass: userPassword }
      }).then(function successCallback(response) {
          callback(response);
      }).catch(function errorCallback(response) {
          console.log("Unable to login");
      });
	};


  service.Register = function (userName, userEmail, userPassword, callback) {
    $http({
          method: 'POST',
          url: 'http://localhost/ideaboardz/api/register.php',
          data: { name: userName,
                  email: userEmail,
                  pass: userPassword,
                  role: 'user' }
      }).then(function successCallback(response) {
          callback(response);
      }).catch(function errorCallback(response) {
          console.log("Unable to register");
      });
  };



  service.setCredentials = function(userEmail, userName, id, role) {
    currentUser = {
      isUserAuthenticated: true,
      useremail: userEmail,
      userName: userName,
      id: id,
      role: role
    }
  };



  service.setCookie = function(userEmail, token, userName, id, role) {
   var credentials = {
      useremail: userEmail,
      username: userName,
      id: id,
      token: token,
      role: role
    };

    var date = new Date();
    date.setDate(date.getDate() + 1);

    $cookies.putObject('credentials', credentials, {'expires': date});
  };


  service.isUserAuthenticated = function() {
    return currentUser.isUserAuthenticated;
  };


  service.getUser = function() {
    if($cookies.getObject('credentials')) {
      var name = $cookies.getObject('credentials').username;
      return name;
    }
  };


  service.getUserId = function() {  
    if($cookies.getObject('credentials')) {
      var id = $cookies.getObject('credentials').id;
      return id;
    }
  };


  service.getUserRole = function() {   
    if($cookies.getObject('credentials')) {
      var role = $cookies.getObject('credentials').role;
      return role;
    }
  };

  service.getUserEmail = function() {  
    if($cookies.getObject('credentials')) {
      var email = $cookies.getObject('credentials').useremail;
      return email;
    }
  };


  service.clearCredentials = function() {
    currentUser = {
      isUserAuthenticated: false,
      useremail: ""
    }
    $cookies.remove('credentials');
  };


  service.confirmToken = function( token ) {
    $http({
          method: 'POST',
          url: 'http://localhost/ideaboardz/api/confirm_token.php',
          data: { userToken: token}
    }).then(function successCallback(response) {
          var confirmToken = JSON.parse(response.data);

          if(confirmToken) {
            var currentUser = $cookies.getObject('credentials').useremail;
            service.setCredentials(currentUser);
            $location.path('/home');
          } else {
            service.clearCredentials();
            $location.path('/navigation');
          }
    }).catch(function errorCallback(response) {
          console.log("Unable to confirmToken");
    });
  };


  service.updatePassword = function(userEmail , updatePass, updatePassNew) {
    return $http({
          method: 'POST',
          url: 'http://localhost/ideaboardz/api/updatePassword.php',
          data: { email : userEmail,
                  password: updatePass,
                  passwordNew: updatePassNew }
    });
  };


return service;

}]);