'use strict';

angular.module('Home').factory('DataTrans', function () {
  var service = {};

    var toMove = {
      fromTable: "",
      toTable: "",
      id: "",
      text: "",
      reporter: ""
    };

    service.setMoved = function (fromTable, toTable, id, text, reporter) {
      toMove.fromTable = fromTable;
      toMove.toTable = toTable;
      toMove.id = id;
      toMove.text = text;
      toMove.reporter = reporter;
    };

    service.getMoved = function () {
      return toMove;
    };

  return service;
});
