'use strict';

angular.module('Home').directive('draggable', ['MainService' , function(MainService) {

    var currentUserID = MainService.getUserId();
// console.log(currentUserID);


    return {
      link: function(scope, item) {
          // this gives us the native JS object
          var el = item[0];

          el.draggable = true;

          el.addEventListener(
              'dragstart',
              function(e) {
                if (e.target.nodeName != '#text') {
                    var isElementMovedable = e.target.attributes['data-transfer'] != null;
                    console.log(isElementMovedable); 
                    if (isElementMovedable) {
                        console.log('data set');
                        var data = JSON.parse(e.target.attributes['data-transfer'].nodeValue);                 
                        var fromTable = e.target.parentNode.parentNode.attributes['data-table'].nodeValue;
                        e.dataTransfer.setData('id', data.id);
                        e.dataTransfer.setData('text', data.text);
                        e.dataTransfer.setData('reporter', data.reporter);
                        e.dataTransfer.setData('fromTable', fromTable);
                    } 
                }              
              },
              false
          );
      }
    }

}])

.directive('dropable', [ 'DataTrans' , function(DataTrans) {
    return {
        scope: {
            drop: '&' // bind to parent scope
        },
        link: function(scope, item) {
            // this gives us the native JS object
            var el = item[0];

            el.addEventListener(
                'dragover',
                function(e) {                                        
                    e.preventDefault();
                    return false;                                     
                },
                false
            );

            el.addEventListener(
                'drop',
                function(e) {                    
                    if(e.dataTransfer.getData('fromTable') != ""){
                        e.preventDefault();
                        e.stopPropagation();
                        var fromTable = e.dataTransfer.getData('fromTable');
                        var toTable = el.attributes['data-table'].nodeValue;
                        var id = e.dataTransfer.getData('id');
                        var text = e.dataTransfer.getData('text');
                        var reporter = e.dataTransfer.getData('reporter');

                        DataTrans.setMoved(fromTable, toTable, id, text, reporter);
                        scope.$apply('drop()');
                        return false;
                    } 
                },
                false
            );
        }
    }
}]);
