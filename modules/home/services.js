'use strict';

angular.module('Home').factory('Database' , [ '$http' , function($http) { 

	var service = {};

	service.addData = function(message, currentUserID, nameTable) {
		return  $http({
			        method: 'POST',
			        url: 'http://localhost/ideaboardz/api/addData.php',
			        data: { message: message,
			                userID: currentUserID,
			                table: nameTable }
			    });
	};


	service.getData = function() { 
		return  $http({
		            method: 'GET',
		            url: 'http://localhost/ideaboardz/api/readData.php'
		        });
	};


	service.removeData = function(tableNumber, id) {
		return  $http({
			        method: 'POST',
			        url: 'http://localhost/ideaboardz/api/removeData.php',
			        data: { table: tableNumber,
			                itemId: id }
			    });			
	};


	service.updateMessage = function(tableNumber , id , text) {
		return  $http({
			        method: 'POST',
			        url: 'http://localhost/ideaboardz/api/updateData.php',
			        data: {	table : tableNumber,
			        		id : id,
			            	text: text }
		    	});
	};


    service.moveData = function (fromTable, toTable, id, text, reporter) {
        return $http({
         			method: 'POST',
         			url:'http://localhost/ideaboardz/api/moveData.php', 
         			data: { FromTable: fromTable, 
         					ToTable: toTable, 
         					Id:id, 
         					Message: text, 
         					Reporter: reporter}
         	    });
    };


return service;

}]);