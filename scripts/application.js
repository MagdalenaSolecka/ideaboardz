'use strict';

angular.module('Navigation', [] );
angular.module('Home', [] );

var app = angular.module('application' , [ 'ngRoute' , 'Navigation' , 'Home' , 'ngCookies'] );

app.config( ['$routeProvider' , '$locationProvider', '$qProvider' , function( $routeProvider, $locationProvider, $qProvider) {
$qProvider.errorOnUnhandledRejections(false);
$locationProvider.hashPrefix('');


	$routeProvider
		.when('/navigation' , {
			controller: 'NavigationController',
			templateUrl: 'modules/navigation/views/navigation.html'
		})

		.when('/login' , {
			controller: 'LoginController',
			templateUrl: 'modules/navigation/views/login.html'
		})

		.when('/register' , {
			controller: 'RegisterController',
			templateUrl: 'modules/navigation/views/register.html'
		})

		.when('/home' , {
			controller: 'HomeController',
			templateUrl: 'modules/home/views/home.html'
		})

		.otherwise( {
			redirectTo: '/navigation'
		});
}]);

app.run( [ '$rootScope' , '$location' , 'MainService' , '$cookies' , '$http' , 'Database' , function( $rootScope, $location, MainService, $cookies, $http, Database ) {

	if($cookies.getObject('credentials')) {
		var token = $cookies.getObject('credentials').token;
		MainService.confirmToken(token);
	};

	$rootScope.$on( '$locationChangeStart' , function(event, next) {
		
		if ( $location.path() !== '/login' && !MainService.isUserAuthenticated() ) {			
			$location.path('/navigation');
		} else if ( MainService.getUserRole() !== 'admin' && $location.path() == '/register') {
			$location.path('/navigation');
		}
	});

}]);