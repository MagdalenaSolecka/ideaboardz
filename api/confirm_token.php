<?php 

require_once ('token.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$userToken = $request->userToken;

if ($userToken == $token) {
	echo json_encode(true);
} else {
	echo json_encode(false);
}