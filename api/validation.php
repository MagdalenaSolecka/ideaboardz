<?php

class Validation {
	public $success;
	public $errorName;
	public $errorEmail;
	public $errorPass;

	public function __construct($bool, $errorName, $errorEmail, $errorPass) {
		if($bool == true) {
			$this->success = true;
			$this->errorName = "";
			$this->errorEmail = "";
			$this->errorPass = "";
		} else {
			$this->success = false;
			$this->errorName = $errorName;
			$this->errorEmail = $errorEmail;
			$this->errorPass = $errorPass;		
		}
	}
}