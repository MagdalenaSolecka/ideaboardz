<?php

require_once ('init.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$fromTable = $request->FromTable;
$toTable = $request->ToTable;
$id = $request->Id;
$message = $request->Message;
$reporter = $request->Reporter;

//delete element
switch ($fromTable) {
    case 0:
        $Database->query("DELETE FROM todo WHERE id='$id'");
        break;
    case 1:
        $Database->query("DELETE FROM toimprove WHERE id='$id'");
        break;
    case 2:
        $Database->query("DELETE FROM wentwell WHERE id='$id'");
        break;
}

//add element
switch ($toTable) {
    case 0:
        $Database->query("INSERT INTO todo (text, reporter) VALUES ('$message','$reporter')");
        break;
    case 1:
        $Database->query("INSERT INTO toimprove (text, reporter) VALUES ('$message','$reporter')");
        break;
    case 2:
        $Database->query("INSERT INTO wentwell (text, reporter) VALUES ('$message','$reporter')");
        break;
}

//fetch updated data from database
$stmtTodo = $Database->query("SELECT * FROM todo");
$stmtToimprove = $Database->query("SELECT * FROM toimprove");
$stmtWentwell = $Database->query("SELECT * FROM wentwell");

//close connection
$Database->close();

$resultToimprove = array();
while($row = $stmtToimprove->fetch_assoc()) {
  array_push($resultToimprove, $row);
}

$resultTodo = array();
while($row = $stmtTodo->fetch_assoc()) {
  array_push($resultTodo, $row);
}

$resultWentwell = array();
while($row = $stmtWentwell->fetch_assoc()) {
  array_push($resultWentwell, $row);
}

//create one table with all messages
$allMessages = array();
array_push($allMessages, $resultTodo);
array_push($allMessages, $resultToimprove);
array_push($allMessages, $resultWentwell);

echo json_encode($allMessages);
