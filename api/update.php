<?php

class Update {
	public $success;
	public $error;
	public $message;

	public function __construct($bool, $errorPass) {
		if($bool == true) {
			$this->success = true;
			$this->message = "Password is saved!";
		} else {
			$this->success = false;
			$this->message = $errorPass;		
		}
	}
}