<?php

class Check {
	public $success;
	public $id;
	public $userName;
	public $message;
	public $token;
	public $role;

	public function __construct($bool, $id, $userName, $token, $role) {
		if($bool == true) {
			$this->success = true;
			$this->id = $id;
			$this->userName = $userName;
			$this->message = "";
			$this->token = $token;
			$this->role = $role;
		} else {
			$this->success = false;
			$this->id = $id;
			$this->userName = $userName;
			$this->message = "E-mail or Password is invalid";
			$this->token = $token;			
			$this->role = $role;			
		}
	}
}