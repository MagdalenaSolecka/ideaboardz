<?php

require_once ('init.php');
require_once ('update.php');

$postdata = file_get_contents('php://input');
$request = json_decode($postdata);

$email = $request->email;
$password = $request->password;
$passwordNew = $request->passwordNew;

$errorPass = '';
$set = true;

// check password if it exist in database and if provided password is the same as the old password
$stmtPass = $Database->query("SELECT password FROM users WHERE email='$email' ");

if($stmtPass->num_rows>0) {
	$row = $stmtPass->fetch_assoc(); 
	$oldPass = $row['password'];
		if($oldPass != $password) {
		 	$set = false;
		 	$errorPass = 'Password is invalid!';
		} else {
		 	$set = true;
		}
};

// check length password
if((strlen($password))<7 || (strlen($password))>20)
{
	$set = false;
	$errorPass = "Password must have 7-20 characters";
};

if((strlen($passwordNew))<7 || (strlen($passwordNew))>20)
{
	$set = false;
	$errorPass = "Password must have 7-20 characters";
};


// check if password is the same as the old password
if($password == $passwordNew)
{
	$set = false;
	$errorPass = "Password must be different!";
};
	

if($set == true)
{
	// update password
	$stmt = $Database->query(
		sprintf("UPDATE users SET password='%s' WHERE email='$email' ",
		mysqli_real_escape_string($Database, $passwordNew)));

	$response = new Update(true, "");
} else if($set == false) {
	$response = new Update(false, $errorPass);
}


// close connection
$Database->close();

echo json_encode($response);