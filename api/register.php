<?php

require_once ('init.php');
require_once ('check.php');
require_once ('validation.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


$name = $request->name;
$email = $request->email;
$pass = $request->pass;
$role = $request->role;

$errorName = '';
$errorEmail = '';
$errorPass = '';

// ====================== Validation =============================

	$set = true;

	if((strlen($name)<3) || (strlen($name)>15))
	{
		$set = false;
		$errorName = "Name must have 3-15 characters";
	};

	$emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

	if((filter_var($emailB, FILTER_VALIDATE_EMAIL)==false) || ($emailB!=$email))
	{
		$set = false;
		$errorEmail = "Provide a valid email address";
	};


	$stmt = $Database->query(
		sprintf("SELECT id FROM users WHERE email='%s' ",
		mysqli_real_escape_string($Database, $email)));
	if($stmt->num_rows>0) 
	{
		$set = false;
		$errorEmail = "This e-mail exist in the database";
	}

	if((strlen($pass))<7 || (strlen($pass))>20)
	{
		$set = false;
		$errorPass = "Password must have 7-20 characters";
	}

	// $pass_hash = password_hash($pass, PASSWORD_DEFAULT);
	

// final condition
	if($set == true) {
		$Database->query(
			sprintf("INSERT INTO users (name, email, password, role) VALUES ('%s', '%s', '%s', '%s')",
			mysqli_real_escape_string($Database, $name),
			mysqli_real_escape_string($Database, $email),
			mysqli_real_escape_string($Database, $pass),
			mysqli_real_escape_string($Database, $role)));

		$response = new Validation(true, "", "", "");

	} else {
		$response = new Validation(false, $errorName, $errorEmail, $errorPass);
	};


// close connection
$Database->close();

echo json_encode($response);
