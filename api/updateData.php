<?php

require_once ('init.php');

$postdata = file_get_contents('php://input');
$request = json_decode($postdata);

$table = $request->table;
$id = $request->id;
$text = $request->text;


// update element
switch($table) {
	case 0:
		$Database->query(
			sprintf("UPDATE todo SET text='%s' WHERE id='$id'",
			mysqli_real_escape_string($Database, $text)));
		break;
	case 1:
		$Database->query(
			sprintf("UPDATE toimprove SET text='%s' WHERE id='$id'",
			mysqli_real_escape_string($Database, $text)));
		break;
	case 2:
		$Database->query(
			sprintf("UPDATE wentwell SET text='%s' WHERE id='$id'",
			mysqli_real_escape_string($Database, $text)));
		break;
}


//fetch updated data from database
$stmtToDo = $Database->query("SELECT * FROM todo");
$stmtToImprove = $Database->query("SELECT * FROM toimprove");
$stmtWentWell = $Database->query("SELECT * FROM wentwell");


// close connection
$Database->close();



$resultToDo = array();
while($row = $stmtToDo->fetch_assoc())
{
	array_push($resultToDo, $row);
}

$resultToImprove = array();
while($row = $stmtToImprove->fetch_assoc())
{
	array_push($resultToImprove, $row);
}

$resultWentWell = array();
while($row = $stmtWentWell->fetch_assoc())
{
	array_push($resultWentWell, $row);
}


//create one table with all messages
$allMessages = array();
array_push($allMessages, $resultToDo);
array_push($allMessages, $resultToImprove);
array_push($allMessages, $resultWentWell);



echo json_encode($allMessages);