<?php

require_once('init.php');


// get elements
$stmtToDo = $Database->query("SELECT * FROM todo");
$stmtToImprove = $Database->query("SELECT * FROM toimprove");
$stmtWentWell = $Database->query("SELECT * FROM wentwell");


//close connection
$Database->close();



$resultToDo = array();
while($row = $stmtToDo->fetch_assoc()) {
  array_push($resultToDo, $row);
};

$resultToImprove = array();
while($row = $stmtToImprove->fetch_assoc()) {
  array_push($resultToImprove, $row);
};

$resultWentWell = array();
while($row = $stmtWentWell->fetch_assoc()) {
  array_push($resultWentWell, $row);
};



//create one table with all messages
$allMessages = array();
array_push($allMessages, $resultToDo);
array_push($allMessages, $resultToImprove);
array_push($allMessages, $resultWentWell);

echo json_encode($allMessages);