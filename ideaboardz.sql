-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 09 Lut 2018, 20:01
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `ideaboardz`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `todo`
--

CREATE TABLE `todo` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  `reporter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `todo`
--

INSERT INTO `todo` (`id`, `text`, `reporter`) VALUES
(66, 'Julia', 12),
(85, 'notatka Stasi', 21);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `toimprove`
--

CREATE TABLE `toimprove` (
  `id` int(11) NOT NULL,
  `text` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `reporter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `toimprove`
--

INSERT INTO `toimprove` (`id`, `text`, `reporter`) VALUES
(27, 'Magdalena update', 22);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin01', 'admin'),
(12, 'Julia', 'testerek@test.pl', 'lkjhgfd', 'user'),
(17, 'Marcin', 'marcin@gmail.com', 'marcinmarcin', 'user'),
(21, 'Stasia', 'stasia@gmail.com', 'stasia1', 'user'),
(22, 'Magdalena', 'magda@gmail.com', 'magdalena', 'user'),
(36, 'Marek', 'marek@gmail.com', '$2y$10$dLlbD9//FAUo6TKZpizn7OsNHzrCwptY2miRt2nqq6Me8fJ6Ioavu', 'user'),
(37, 'Szymon', 'szymon@gmail.com', '$2y$10$4graTQHSqQrbwUefMtAF9evuKI5kfJT8n4U4g8WeyPwCZxdWSCfS.', 'user'),
(38, 'Hubert', 'hubert@gmail.com', 'hubert5', 'user'),
(39, 'Damian', 'damian@gmail.com', 'mnbvcxz', 'user');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wentwell`
--

CREATE TABLE `wentwell` (
  `id` int(11) NOT NULL,
  `text` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `reporter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `wentwell`
--

INSERT INTO `wentwell` (`id`, `text`, `reporter`) VALUES
(37, 'Hello everyone! I\'m Admin.', 1),
(52, 'I\'m Szymon.', 37);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toimprove`
--
ALTER TABLE `toimprove`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wentwell`
--
ALTER TABLE `wentwell`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `todo`
--
ALTER TABLE `todo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT dla tabeli `toimprove`
--
ALTER TABLE `toimprove`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT dla tabeli `wentwell`
--
ALTER TABLE `wentwell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
